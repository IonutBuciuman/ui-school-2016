'use strict';

module.exports = function(code, message) {

  var err = new Error();
  err.code = code;
  err.message = message;
  
  return err;
}
