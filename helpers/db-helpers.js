'use strict';

var dbHelpers = {};

function getType(param) {
    return Object.prototype.toString.call(param).slice(8, -1).toLowerCase();
    // returns 'object', 'array', 'string', 'number', etc.
}

dbHelpers.castIdToCode = function castIdToCode(results) {

    if (results && results._id && getType(results) === 'object') {
        results.code = results._id;
        delete results._id;

        return results;

    } else if (results && getType(results) === 'array') {
        var formattedResults = [];

        results.forEach(function(val, key, results) {
            if (val._id) {
                val.code = val._id;
                delete val._id;
                formattedResults.push(val);
            }
        });

        return formattedResults;
    } else {
        return results;
    }
};


module.exports = dbHelpers;
