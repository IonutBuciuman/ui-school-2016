'use strict';

module.exports = function errorHandler(err, req, res, next) {
  if (err) {
    switch (err.code) {

      case 11000:
        res
          .status(409)
          .json({
            code: 'CONFLICT',
            message: 'Document already exists'
          });
        return;

      case 400:
        res
          .status(400)
          .json({
            code: 'BAD_REQUEST',
            message: err.message
          });
        return;

      case 404:
        res
          .status(404)
          .json({
            code: 'NOT_FOUND',
            message: err.message || 'Document does not exist'
          });
        return;

      default:
        return next(err);
    }
  } else {
    next();
  }
}
