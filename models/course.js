/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict';

var mongoose = require('mongoose');
var monguurl = require('monguurl');
var Schema = mongoose.Schema;

var CourseSchema = new Schema({
    code: {
        type: String,
        unique: true,
        dropDups: true,
        index: true
    },
    title: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
        default: ""
    },
    location: {
        location_code: {
            type: String,
            required: true
        },
        location_name: {
            type: String,
            required: true
        }
    },
    category: {
        category_code: {
            type: String,
            required: true
        },
        category_name: {
            type: String,
            required: true
        }
    },
    subcategory: {
        subcategory_code: {
            type: String,
            required: true
        },
        subcategory_name: {
            type: String,
            required: true
        }
    },
    votes: {
        type: Number,
        default: 0
    },
    author: {
        user_code: {
            type: String,
            required: true
        },
        username: {
            type: String,
            required: true
        }
    },
    posted: {
        type: String,
        //validated: ,    // would be a timestamp/epochtime
        required: true
    }
});

CourseSchema.plugin(monguurl({
    source: 'title',
    target: 'code'
}));

CourseSchema.methods.incrementVotes = function incrementVotes(callback){
    var query = { $inc: { votes: 1 } };
    return this.update(query, callback);
};
CourseSchema.methods.decrementVotes = function decrementVotes(callback){
    var query = { $inc: { votes: -1 } };
    return this.update(query, callback);
};

module.exports = mongoose.model('Course', CourseSchema);
