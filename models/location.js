/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict';

var mongoose = require('mongoose');
var monguurl = require('monguurl');
var Schema = mongoose.Schema;

var LocationSchema = new Schema({
    code: {
        type: String,
        unique: true,
        dropDups: true,
        index: true
    },
    name: {
        type: String,
        trim: true,
        required: true
    }
});

LocationSchema.plugin(monguurl({
    source: 'name',
    target: 'code'
}));

module.exports = mongoose.model('Location', LocationSchema);