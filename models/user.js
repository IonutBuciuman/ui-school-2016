/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict'

var mongoose = require('mongoose');
var monguurl = require('monguurl');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    code: {
        type: String,
        unique: true,
        dropDups: true,
        index: true
    },
    username: {
        type: String,
        trim: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    created: {
        type: String,
        required: true
    },
    courses: [{
        course_code: {
            type: String,
            required: true
        },
        course_name: {
            type: String,
            required: true
        }
    }]
});

UserSchema.plugin(monguurl({
    source: 'username',
    target: 'code'
}));


module.exports = mongoose.model('User', UserSchema);
