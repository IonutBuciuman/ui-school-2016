'use strict';

var mongoose = require('mongoose');
var monguurl = require('monguurl');
var Schema = mongoose.Schema;

var ProjectSchema = new Schema({
    code: {
        type: String,
        unique: true,
        dropDups: true,
        index: true
    },
    title: {
        type: String,
        trim: true,
        required: true
    },
    description: String,
    location: String,
    category_code: {
        type: String,
        required: true
    },
    votes: {
        type: Number,
        default: 0
    }
});

ProjectSchema.plugin(monguurl({
    source: 'title',
    target: 'code'
}));

ProjectSchema.methods.incrementVotes = function incrementVotes(cb) {
    return this.update({ $inc: { votes: 1 }}, cb);
};

ProjectSchema.methods.decrementVotes = function decrementVotes(cb) {
    return this.update({ $inc: { votes: -1 }}, cb);
};


module.exports = mongoose.model('Project', ProjectSchema);
