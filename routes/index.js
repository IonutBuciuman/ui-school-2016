'use strict';

var apiController = require('controllers/api-controller');

var categoryController = require('controllers/category-controller');
var projectController = require('controllers/project-controller');

var CoursesController = require('controllers/courses-controller');
var CategoriesController = require('controllers/categories-controller');
var SubcategoriesController = require('controllers/subcategories-controller');
var UsersController = require('controllers/users-controller');
var LocationsController = require('controllers/locations-controller');


module.exports = function router(express) {

  var router = new express.Router();

  // API info routes
  router.route('')
      .get(apiController.getInfo);

  // Categories routes
  router.route('/categories')
      .post(categoryController.postCategory)
      .get(categoryController.getCategories);

  router.route('/categories/:code')
     .get(categoryController.getCategoryByCode);

  router.route('/categories/:code/projects')
      .get(projectController.getProjectsByCategory);


  // Projects routes
  router.route('/projects')
      .post(projectController.postProject)
      .get(projectController.getProjects);

  router.route('/projects/:code')
      .get(projectController.getProjectByCode)
      .patch(projectController.updateProject)
      .delete(projectController.deleteProject);

  router.route('/projects/:code/votes')
      .get(projectController.getVotes)
      .post(projectController.like)
      .delete(projectController.dislike);


  return router;
};


