/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict'

var Subcategory = require('models/subcategory');
var error = require('helpers/custom-error');


var SubcategoriesService = {};

SubcategoriesService.getSubcategories = function getSubcategories(request, response, error){

};

SubcategoriesService.getSubcategoryByCode = function getSubcategoryByCode(request, response, error){

};
SubcategoriesService.getSubcategoryByName = function getSubcategoryByName(request, response, error){

};

SubcategoriesService.createSubcategory = function createSubcategory(request, response, error){

};

SubcategoriesService.updateSubcategoryName = function updateSubcategoryName(request, response, error){

};

SubcategoriesService.deleteSubcategory = function deleteSubcategory(request, response, error){

};


module.exports = SubcategoriesService;