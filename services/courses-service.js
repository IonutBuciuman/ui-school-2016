/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict';

var Course = require('models/course');
var Location = require('models/location');
var Category = require('models/category');
var Subategory = require('models/subcategory');
var User = require('models/user');

var error = require('helpers/custom-error');
var LocationsService = require('services/locations-service');

var q = require('q');

var CoursesService = {};


CoursesService.getCourses = function getCourses(){};

CoursesService.getCourseByCode = function getCourseByCode(code){};
CoursesService.getCourseByTitle = function getCourseByTitle(title){};

CoursesService.createCourse = function createCourse(course){};

CoursesService.updateCourse = function updateCourse(newCourse){};
CoursesService.updateCourseTitle = function updateCourseTitle(newTitle){};
CoursesService.updateCourseDescription = function updateCourseDescription(newDescription){};

CoursesService.deleteCourse = function deleteCourse(code){};

CoursesService.voteupCourse = function voteupCourse(code){};
CoursesService.votedownCourse = function votedownCourse(code){};


module.exports = CoursesService;
