/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict';

var Category = require('models/category');
var error = require('helpers/custom-error');

var q = require('q');

var CategoriesService = {};

CategoriesService.getCategories = function getCategories(request, response, error){

};

CategoriesService.getCategoryByCode = function getCategoryByCode(request, response, error){

};
CategoriesService.getCategoryByName = function getCategoryByName(request, response, error){

};

CategoriesService.createCategory = function createCategory(request, response, error){

};

CategoriesService.updateCategoryName = function updateCategoryName(request, response, error){

};

CategoriesService.deleteCategory = function deleteCategory(request, response, error){

};


module.exports = CategoriesService;