/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict';

var User = require('models/user');
var Course = require('models/course');
var CoursesService = require('services/categories-service');
var error = require('helpers/custom-error');

var q = require('q');

var UsersService = {};

UsersService.getUsers = function getUsers(request, response, error){

};

UsersService.getUserByCode = function getUserByCode(request, response, error){

};
UsersService.getUserByUsername = function getUserByUsername(request, response, error){

};


UsersService.createUser = function createUser(request, response, error){

};

UsersService.changePassword = function changePassword(request, response, error){

};

UsersService.addCourse = function addCourse(request, response, error){

};

UsersService.removeCourse = function removeCourse(request, response, error){

};

UsersService.deleteUser = function deleteUser(request, response, error){

};


module.exports = UsersService;
