/**
 * Created by ibuciuman on 4/12/2016.
 */
'use strict'

var Location = require('models/location');
var error = require('helpers/custom-error');

var q = require('q');

var LocationsService = {};

LocationsService.getLocations = function getLocations(request, response, error){

};

LocationsService.getLocationByCode = function getLocationByCode(request, response, error){

};
LocationsService.getLocationByName = function getLocationName(request, response, error){

};

LocationsService.createLocation = function createLocation(request, response, error){

};

LocationsService.updateLocationName = function updateLocationName(request, response, error){

};

LocationsService.deleteLocation = function deleteLocation(request, response, error){

};


module.exports = LocationsService;