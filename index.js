'use strict';

require('app-module-path').addPath(__dirname);

var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = require('config');
var routes = require('routes')(express);
var errorHandler = require('helpers/error-handler');
var app = express();


// Database connection
function connectDb() {
    return new Promise(function(resolve, reject) {
        mongoose.connect(config.database, function mongooseConnectionCb(err) {
            if (err) {
                reject(err);
            } else {
                resolve({
                    success: true,
                    message: 'Connected to the database: ' + config.database
                });
            }
        });
    });
}

// Add middlewares and routes to Express
function addApi(app) {

    // Enable CROSS
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    // Express middlewares
    app.use(bodyParser.urlencoded({
       extended: true
    }));
    app.use(bodyParser.json());

    // API routes
    app.use('', routes);

    // Error handler
    app.use(errorHandler);

}

// Start express server
function startServer(app) {
    app.listen(config.port, function() {
        console.log('Server is running on: ' + config.url + ':' + config.port);
    });
}


// Bootstrap app
connectDb()
  .then(function(res) {
    console.log(res.message);
    addApi(app);
  })
  .then(function() {
    startServer(app);
  })
  .catch(function(err) {
    console.error(err);
});
