'use strict';

function config() {
    //var mongodb_url = 'mongodb://ui-ux:ui-ux@ds035985.mongolab.com:35985/ui-ux';
    var mongodb_url = 'mongodb://cikunu:1234@ds057204.mlab.com:57204/elearning';

    switch (process.env.NODE_ENV) {

        case 'development':
            return {};

        case 'production':
            return {
                url: process.env.HOST,
                database: mongodb_url,
                port: process.env.PORT
            };

        default:
            return {
                url: 'http://localhost',
                database: mongodb_url,
                port: '5000'
            };
    }
}

module.exports = config();
