'use strict';

var fs = require('fs');

var apiController = {};

apiController.getInfo = function getInfo(req, res, next) {

  var packageJson = fs.readFile('package.json', 'utf8', function readFileCb(err, file) {
      var info = {};

      if (err) {
          next(err);
      } else {
          file = JSON.parse(file);
          info.name = file.name;
          info.version = file.version;
          info.description = file.description;
          info.author = file.author;

          info.endpoints = {
              "/categories": "GET, POST",
              "/categories/:code": "GET",
              "/categories/:code/projects": "GET",

              "/projects": "GET, POST",
              "/projects/:code": "GET, PATCH, DELETE",
              "/projects/:code/votes": "GET, POST, DELETE"

        };

        res.send(info);
    }
  });
};

module.exports = apiController;
