'use strict';

//var Category = require('models/category');
var error = require('helpers/custom-error');
var dbHelpers = require('helpers/db-helpers');

var categoryController = {};

categoryController.postCategory = function postCategory(req, res, next) {
    var _err;
    
    if (req.body.title) {
        var newCategory = new Category({
            title: req.body.title,
            description: req.body.description
        });
        
        newCategory.save(function saveNewCategory(err, category) {
            if (err) {
                return next(err);
            } else {
                res
                    .status(201)
                    .json({
                        success: true,
                        message: 'Category saved',
                        code: category.code
                    });
            }
        });
        
    } else {
        _err = new error(400, 'Title is required');
        
        return next(_err);
    }
};

categoryController.getCategories = function getCategories(req, res, next) {
    
    Category.find({}, {
        __v: 0,
        _id: 0
    }).exec( function findCategories(err, categories) {
        
        if (err) {
            return next(err);
        } else {
            res.json(categories);
        }
    });
    
};

categoryController.getCategoryByCode = function getCategoryByCode(req, res, next) {
    
    Category.findOne({
        code: req.params.code
    }, {
        __v: 0,
        _id: 0
    }, function findCategoryByCode(err, category) {
        
        var _err;
        if (err) {
            return next(err);
        } else if (!category || category.length <= 0) {
            _err = new error(404);
            return next(_err);
        } else {
            res.json(category);
        }
    });
};

module.exports = categoryController;
