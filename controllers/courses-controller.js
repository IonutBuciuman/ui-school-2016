/**
 * Created by DoomSayer on 5/2/2016.
 */
'use strict';

var Course = require('models/course');
// var Category = require('models/category');
var Subcategory = require('models/subcategory');

var CoursesService = require('services/courses-service');

var q = require('q');

var CoursesController = {};

CoursesController.getCourses = function getCourses(req, res, error){};
CoursesController.getCourseByCode = function getCourseByCode(req, res, error){};
CoursesController.getCourseByTitle = function getCourseByTitle(req, res, error){};

CoursesController.updateCourseTitle = function updateCourseTitle(req, res, error){};
CoursesController.updateCourseDescription = function updateCourseDescription(req, res, error){};
CoursesController.updateCourseLocation = function updateCourseLocation(req, res, error){};
CoursesController.updateCourseCategory = function updateCourseCategory(req, res, error){};
CoursesController.updateCourseSubcategory = function updateCourseSubcategory(req, res, error){};

CoursesController.createCourse = function createCourse(req, res, error){};

CoursesController.deleteCourse = function deleteCourse(req, res, error){};

CoursesController.voteupCourse = function voteupCourse(req, res, error){};
CoursesController.votedownCourse = function votedownCourse(req, res, error){};


module.exports = CoursesController;
