'use strict';

var Project = require('models/project');
//var Category = require('models/category');
var error = require('helpers/custom-error');
var dbHelpers = require('helpers/db-helpers');

var projectController = {};

function _checkCategoryAsync(req) {
    
    return new Promise(function categoryPromiseCb(resolve, reject) {
        
        Category.findOne({
            code: req.body.category_code
        }, {
            code: 1
        }, function getCategoryIdByCode(err, category) {
            var _err;
            
            if (err) {
                reject(err);
            } else if (!category || category.length <= 0) {
                _err = new error(404, 'Invalid category code');
                reject(_err);
            } else {
                resolve(category);
            }
        });
    });
};

projectController.postProject = function postProject(req, res, next) {
    var _err;
    
    if (req.body.title && req.body.category_code) {
        
        _checkCategoryAsync(req).then(function(category) {
            var newProject = new Project({
                title: req.body.title,
                description: req.body.description,
                location: req.body.location,
                category_code: req.body.category_code
            });
            
            newProject.save(function saveNewProject(err, project) {
                if (err) {
                    return next(err);
                } else {
                    res.status(201)
                        .json({
                            success: true,
                            message: 'Project saved',
                            code: project.code
                        });
                }
            });
        }).catch( function(err) {
            return next(err);
        });
        
    } else {
        _err = new error(400, 'Title and category code are required');
        return next(_err);
    }
};

projectController.getProjects = function getProjects(req, res, next) {
    Project.find({}, {
        __v: 0,
        _id: 0
    }).exec(function findProjects(err, projects) {
        if (err) {
            return next(err);
        } else {
            res.json(projects);
        }
    });
};

projectController.getProjectByCode = function getProjectByCode(req, res, next) {
    
    Project
        .findOne({
            code: req.params.code
        }, {
            __v: 0,
            _id: 0
        })
        .exec(function findProjectByCodeCb(err, project) {
            var _err;
            if (err) {
                return next(err);
            } else if (!project || project.length <= 0) {
                _err = new error(404);
                return next(_err);
            } else {
                res.json(project);
            }
        });
};

projectController.updateProject = function updateProject(req, res, next) {
    
    var update = {};
    var updatableFields = ['description', 'location'];
    
    function _updateProject(updateObj) {
        Project
            .findOne({
                code: req.params.code
            }, {})
            .update(updateObj)
            .exec(function updateProjectCb(err) {
                if (err) {
                    return next(err);
                } else {
                    res
                        .status(204)
                        .end();
                }
            });
    };
    
    updatableFields.forEach(function(elem, index) {
        if (req.body[elem] || req.body[elem] === '') {
            update[elem] = req.body[elem];
        }
    });
    
    if (req.body.title && req.body.title.trim().length > 0) {
        update.title = req.body.title;
    }
    
    if (req.body.category_code && req.body.category_code.trim().length > 0) {
        _checkCategoryAsync(req).then(function(category) {
            update.category_code = category.code;
            
            _updateProject(update);
            
        }).catch(function(err) {
            return next(err);
        });
    } else {
        _updateProject(update);
    }
    
};

projectController.getProjectsByCategory = function getProjectsByCategory(req, res, next) {
    
    Project
        .find({
            category_code: req.params.code
        }, {
            __v: 0,
            _id: 0
        })
        .exec(function findProjectsByCategoryCb(err, projects) {
            if (err) {
                return next(err);
            } else {
                res.json(projects);
            }
        });
};

projectController.deleteProject = function deleteProject(req, res, next) {
    
    Project
        .findOne({
            code: req.params.code
        })
        .remove()
        .exec(function removeProjectCb(err, project) {
            if (err) {
                return next(err);
            } else {
                res
                    .status(204)
                    .end();
            }
        });
};

projectController.getVotes = function getVotes(req, res, next) {
    
    Project
        .findOne({
            code: req.params.code
        }, {
            __v: 0,
            _id: 0
        })
        .exec(function findProjectVotesCb(err, project) {
            var _err;
            
            if (err) {
                return next(err);
            } else if (!project || project.length <= 0) {
                _err = new error(404);
                return next(_err);
            } else {
                res.json({
                    votes: project.votes
                });
            }
        });
};

projectController.like = function like(req, res, next) {
    
    Project
        .findOne({
            code: req.params.code
        }, {})
        .exec(function findProjectVotesCb(err, project) {
            var _err;
            
            if (err) {
                return next(err);
            } else if (!project || project.length <= 0) {
                _err = new error(404);
                return next(_err);
            } else {
                
                project.incrementVotes(function incrementVotesCb(err) {
                    if (err) {
                        return next(err);
                    } else {
                        res.json({
                            votes: project.votes + 1
                        });
                    }
                });
            }
        });
};

projectController.dislike = function dislike(req, res, next) {
    
    Project
        .findOne({
            code: req.params.code
        }, {})
        .exec(function findProjectVotesCb(err, project) {
            var _err;
            
            if (err) {
                return next(err);
            } else if (!project || project.length <= 0) {
                _err = new error(404);
                return next(_err);
            } else {
                
                project.decrementVotes(function decrementVotesCb(err) {
                    if (err) {
                        return next(err);
                    } else {
                        res.json({
                            votes: project.votes - 1
                        });
                    }
                });
            }
        });
};


module.exports = projectController;
